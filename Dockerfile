FROM rclone/rclone

WORKDIR /tmp
COPY fetchminio.sh .
ENTRYPOINT ["./fetchminio.sh"]
CMD ["./fetchminio.sh"]
